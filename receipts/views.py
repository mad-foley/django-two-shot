from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.
@login_required
def receipt_list(request):
  receipt = Receipt.objects.filter(purchaser=request.user)
  context = {
    'receipt_list': receipt
  }
  return render(request, "receipts/list.html", context)

def redirect_to_receipt_list(request):
  return redirect('home')

@login_required
def create_receipt(request):
  if request.method == "POST":
    form = ReceiptForm(request.POST)
    if form.is_valid():
      receipt = form.save(False)
      receipt.purchaser = request.user
      receipt.save()
      return redirect("home")
  else:
    form = ReceiptForm()

  context = {
    "form": form,
  }
  return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
  categories = ExpenseCategory.objects.filter(owner=request.user)
  context = {
    "categories": categories,
  }
  return render(request, 'categories/list.html', context)

@login_required
def account_list(request):
  accounts= Account.objects.filter(owner=request.user)
  context = {
    "accounts": accounts,
  }
  return render(request, 'accounts/list.html', context)

@login_required
def create_category(request):
  if request.method == 'POST':
    form = ExpenseCategoryForm(request.POST)
    if form.is_valid():
      category = form.save(False)
      category.owner = request.user
      category.save()
      return redirect('category_list')
  else:
    form = ExpenseCategoryForm()

  context = {
    'form': form,
  }
  return render(request, 'categories/create.html', context)

@login_required
def create_account(request):
  if request.method == "POST":
    form = AccountForm(request.POST)
    if form.is_valid:
      acc = form.save(False)
      acc.owner = request.user
      acc.save()
      return redirect('account_list')
  else:
    form = AccountForm()
  context = {
    'form': form,
  }
  return render(request, 'accounts/create.html', context)
